#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int saveData(char Data[][6][32], char name[], double g1, double g2, double g3, double final, char letter[], int nStudent){

	char strg1[32], strg2[32], strg3[32], strfinal[32];
	sprintf(strg1, "%.2lf", g1);
	sprintf(strg2, "%.2lf", g2);
	sprintf(strg3, "%.2lf", g3);
	sprintf(strfinal, "%.2lf", final);

	strcpy(Data[nStudent][0], name);
	strcpy(Data[nStudent][1], strg1);
	strcpy(Data[nStudent][2], strg2);
	strcpy(Data[nStudent][3], strg3);
	strcpy(Data[nStudent][4], strfinal);
	strcpy(Data[nStudent][5], letter);

	return 1;
}

int main(){

	char* token;
	char str[128], name[32], letter[8];
	double grade[3], final;
	char Data[100][6][32];
	int nStudent = 0;

	printf("Usage: <stduent's name> <grade 1> <grade 2> <grade 3>\nData: ");
	fgets(str, sizeof(str), stdin);
	
	size_t strLength = strlen(str) - 1;
	if(str[strLength] == '\n'){ 
		str[strLength] = '\0';
	}

	while(strcmp(str, "quit") ){
		token = strtok(str, " ");
		strcpy(name, token);
		token = strtok(NULL, " ");

		int i = 0;
		for(; i < 3; i++){
			grade[i] = atof(token);
			token = strtok(NULL, " ");
		}
	
		final = grade[0] * 0.3 + grade[1] * 0.3 + grade[2] * 0.4;
	
		if( final > 90){ strcpy(letter,"A"); }
		else if( final > 80){ strcpy(letter,"B"); }
		else if( final > 70){ strcpy(letter,"C"); }
		else if( final > 65){ strcpy(letter,"D"); }
		else{ strcpy(letter,"F"); }
	
		printf("%s : %.2lf, %.2lf, %.2lf [Final: %.2lf] = %s\n", name, grade[0], grade[1], grade[2], final, letter);
	
		nStudent += saveData(Data, name, grade[0], grade[1], grade[2], final, letter, nStudent);
	
		printf("Usage: <stduent's name> <grade 1> <grade 2> <grade 3>\n Data: ");
		fgets(str, sizeof(str), stdin);
	}
	
	int k = 0;
	for(; k < nStudent; k++){
		printf("%s : %s, %s, %s, [Final: %s] = %s\n", Data[k][0],Data[k][1],Data[k][2],Data[k][3],Data[k][4],Data[k][5]);
	}
	return 0;
	
}