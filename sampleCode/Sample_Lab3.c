#include <stdio.h>

unsigned long long factorial(unsigned long long n){

	/*
	for(int i = n-1; i > 1; i--){
		n *= i;
	}

	return n;
	*/
	unsigned long long result = 1;
	for(int i = 2; i <= n; i++){
		result *= i;
	}

	return result;
}//Factorial

unsigned long long bicoeff(unsigned long long n, unsigned long long k){

	unsigned long long num, denom1, denom2, den, result;

	num = factorial(n);
	denom1 = factorial(k);
	denom2 = factorial(n-k);
	den = denom1 * denom2;

	result = num/den;

	return result;
}//Binomial Coefficient

unsigned long long catalan(unsigned long long n){

	unsigned long long bc, result;
	bc = bicoeff(2*n, n);
	result = bc / (n + 1);

	return result;
}//Catalan Number

int main(){

	int choice;

	do{
			// Choice Manual & Prompt
			printf("-------------------\nChoices:\n");
			printf("0: Exit the program\n");
			printf("1: Find the Factorial of n\n");
			printf("2: Find the Combination of n things k at a time\n");
			printf("3: Find the Catalan number of n as defined above\n");
			printf("\nEnter Choice: ");
			scanf("%d", &choice);

			unsigned long long n, k, result;
			switch (choice){

				case 0:
					printf("\nExit\n");
					break;

				case 1:
					printf("\tEnter n : ");
					scanf("%llu", &n);
					result = factorial(n);
					printf("factorial(n) = %llu\n", result);
					break;

				case 2:
					printf("\tEnter n and k: ");
					scanf("%llu%llu", &n, &k);
					result = bicoeff(n, k);
					printf("Combination(n,k) = %llu\n", result);
break;

				case 3:
					printf("\tEnter n: ");
					scanf("%llu", &n);
					result = catalan(n);
					printf("Catalan Number(n) = %llu\n", result);
break;

				default:
					printf("Error: Wrong Choice Input. Please re-try.\n");
					break;

			}//switch(choices)

		}while(choice!=0);

		printf("Thank you for using the program.\n");

	return 0;
}
