#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(){

  FILE *fr;

  // Prompt for a FileName
  char filename[32];
  printf_s("Enter Filename: ");
  fgets(filename, sizeof(filename), stdin);
  if(filename[strlen(filename)-1] == '\n'){ filename[strlen(filename)-1] = '\0'; }//remove newline character at the end of filename string

  char saveBuff[4096]; // temporary buffer/string which holds the data in the file

  // Set File Exist flag
  int existFlag = 1;
  fopen_s(&fr, filename, "r"); // Open a file with a read mode -readFile
  if(fr == NULL){
    existFlag = 0;
    printf_s("Creating file: %s\n", filename); // File NOT Exists
  }else{
    printf_s("Opening file: %s\n", filename); // File Exists

    /* Read the file line by line using fgets */
    char rline[2048];

    strcpy(saveBuff, ""); // initialized the temporary buffer

    printf_s("\n------- Start Reading %s -------\n", filename);
    // Display Existing File Data on the screen
    while(fgets(rline, sizeof(rline), fr)){
      printf_s("%s", rline); // Deisplay data in the file on the screen
      strcat(saveBuff, rline); // Added the currently read line into the temporary saveBuffer
    }
    printf_s("\n------- End Reading %s -------\n", filename);
    fclose(fr); // close readFile
  }

  // Prompt for a Command: Write w or Append a
  char cmd[8];
  printf_s("\nCommand:\n\t'w' to write from top of file\n\t'a' to write to the bottom of file\n%s command: ", filename);
  fgets(cmd, sizeof(cmd), stdin);
  if(cmd[strlen(cmd)-1] == '\n'){cmd[strlen(cmd)-1] = '\0';}//remove newline character at the end of command string

  // Invalid command cmd Handling!
  while(strcmp(cmd, "w") && strcmp(cmd, "a")){

    // Invalid Command Error Message
    printf_s("Error: Invaild Input. Command must be w or q\n\n");
    printf_s("\nCommand:\n\t'w' to write from top of file\n\t'a' to write to the bottom of file\n%s command: ", filename);

    //Get New command cmd input
    fgets(cmd, sizeof(cmd), stdin);
    if(cmd[strlen(cmd)-1] == '\n'){cmd[strlen(cmd)-1] = '\0';} //remove newline character at the end of command string
  }

  /* Open File to read/write/append */
  // Write a File
  if(!strcmp(cmd, "w")){
    FILE *fw;
    fopen_s(&fw, filename, "w+");//open a file with an open+ mode -WriteFile

    char wline[2048];
    printf_s("Start Writing from Top of the File%s\n\n", filename);

    fgets(wline, sizeof(wline), stdin); // Get 1st Keyboard Input

    /* Over-write the existing file */
    /* WHILE user doesnt type newline */
    while(strcmp(wline, "\n")){
      fprintf_s(fw, "%s",wline); // Write into a File
      fgets(wline, sizeof(wline), stdin); // Get Keyboard Input
    }

    fprintf_s(fw, "%s", saveBuff); // append saved Data

    printf_s("End Write 'w'\n");
    fclose(fw);//close WriteFile
  }

  // Append a File
  if(!strcmp(cmd, "a")){
    FILE *fa;
    fopen_s(&fa, filename, "a+");//open a file with an append+ mode -appendFile
    char aline[2048];
    printf_s("Start Writing from Bottom of the File%s\n\n", filename);

    fgets(aline, sizeof(aline), stdin); // Get 1st Keyboard Input

    /* Do NOT over-write the existing file; write after the end of existing data */
    /* WHILE user doesnt type newline */
    while(strcmp(aline, "\n")){
      fprintf_s(fa, "%s",aline); // Apppend into a File
      fgets(aline, sizeof(aline), stdin); // Get Keyboard Input
    }

    printf_s("End Append 'a'\n");
    fclose(fa);//close appendFile
  }

  printf_s("\nExit the program.\n\n");

  return 0;
}
