#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct node {
  int data;
  struct node *next;
};

typedef struct node NODE;

int printList (NODE *root, int count){

  printf("Backward Linked List: ");
  NODE *ptr = root;
  int i;

  for(i = 0; ptr->next != NULL; i++){
    printf("%d >> %d", i, ptr->data);
    ptr = ptr->next;
  }
  printf("NULL\n");
  return 1;
}

NODE * add(int data, NODE *current){

  NODE* new = (NODE*)malloc(sizeof(NODE));
  new->data = data;

  new->next = NULL;

  current->next = new;

  return new;
}

int main(){

  char input[1028], *token;
  int num, count = 0;
  NODE *root;

  printf("\nLinked List:\nType Integers to store in the List:\n");
  printf("INPUT: ");
  fgets(input, sizeof(input), stdin);
  if(input[strlen(input)-1] == '\n'){ input[strlen(input)-1] = '\0'; }

  while(input == '\0'){

    token = strtok(input, " ,-");

    // HEAD/ROOT NODE
    root = (NODE*)malloc(sizeof(NODE));
    root->data = atoi(token);
    root->next = NULL;
    count++;

    NODE *new = (NODE *)malloc(sizeof(NODE));
    token = strtok(NULL, " ");
    while(token != NULL){
      num = atoi(token);
      new->data = num;
      new->next = NULL;
      root = add(root, new);
      count++;
      token = strtok(NULL, " ,-");
    }

    printf("INPUT: ");
    fgets(input, sizeof(input), stdin);
    if(input[strlen(input)-1] == '\n'){ input[strlen(input)-1] = '\0'; }
  }

  //printForward(root);
  printList(root, count);

  printf("\nEND of Program\n\n");
  return 0;
}
