#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node{
	int data;
	struct node *next;
};

typedef struct node Node;
void printStack(Node *first) {

	Node *current = first;
	while(current !=NULL){
		printf("%d ", current->data);	
	current = current->next;
	}
	if(first == NULL){ printf("stack Empty!");}
	printf("\n");
}
Node *pop(Node *first){
	Node* r = first;
	first = first->next;
	r->next = NULL;

	printf("Removing %d: ", r->data);
	printStack(first);
	free(r);
	return first;
}
Node *push(Node *first, Node* new){
  new->next = first;  // first is new!
	first = new;

  return first;
}

int main(void)
{
	int n, data;
	char line[80];
	Node *first= NULL;
	Node *current;

	printf("Enter a number: ");
	fgets(line, 80, stdin); //Read an input line					
  data = atoi(strtok(line, " "));
	
  // STACK CREATED
  if (first == NULL) {	//if a number was read
		first = malloc(sizeof(Node));//Create a node,
		first->data = data; //save data in it,
		first->next = NULL; //and set its link to NULL
	}
  char* token = strtok(NULL, " ");
  //while there is integers in user keyboard-input. read and call PUSH function.
	while (token != NULL) { //If another input was provided
    data = atoi(token);
    Node * new = malloc(sizeof(Node));//Create a node,
		new->data = data; //save data in it,
		new->next = NULL; //new points to NULL

    		first = push(first, new);
    
    token = strtok(NULL, " ");
	}

	printStack(first);
	while(first != NULL){
	first = pop(first);
	
	}	
  
	return 0;
}