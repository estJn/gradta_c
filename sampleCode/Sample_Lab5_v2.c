#include <stdio.h>
#include <stdlib.h>

int VOWEL(char s[]){

  int v = 0;
  for(int i = 0; s[i] != '\0'; i++){
    if(s[i] == 'a' || s[i] == 'A' || s[i] == 'e' || s[i] == 'E' || s[i] == 'i' || s[i] == 'I' || s[i] == 'o' || s[i] == 'O' || s[i] == 'u' || s[i] == 'U'){
      v++;
    }
  }//for(s[i])

  return v;
}//vowel()

int CONSONANT(char s[], int v){

  int ch = 0;
  for(int i = 0; s[i] != '\0'; i++){
    if((s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z')){
      ch++;
    }
  }//for(s[i])

  return (ch - v);

}//vowel()

int main(){

  int v, c;
  char sentence[1024];

  printf("Sentence: ");
  gets(sentence);

  while(sentence[0] != '\0'){
    v = VOWEL(sentence);
    c = CONSONANT(sentence, v);

    printf("VOWEL: %d\tCONSONANT: %d\n\n", v, c);

    printf("Sentence: ");
    gets(sentence);
  }
  printf("Exit the Program.\n");
  return 0;
}
