#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct {
  char firstName[32], lastName[32];
  float ceg, cs;
} STUDENT;

void printTable(STUDENT table[]){
  printf("\n-------------------------\nNO\tFIRST\tLAST\tCEG2170\tCS1180\n");

  int i;
  for(i = 0; i < 10; i++){
    printf("%d\t%s\t%s\t%.2f\t%.2f\n", i+1, table[i].firstName, table[i].lastName, table[i].ceg, table[i].cs);
    }
		printf("\nEND of RECORD\n-------------------------\n\n");
}

int main(){

  char buff[128];
	char* token;
  STUDENT table[10];

	printf("RECORD #: <FristName> <LastName> <CEG2170 Grade> <CS1180 Grade>\n");

  int i;
  for(i = 0; i < 10; i++){
    printf("RECORD %d: ", i+1);
		fgets(buff, sizeof(buff), stdin);
	if(buff[strlen(buff) - 1] == '\n'){ buff[strlen(buff) - 1] = '\0';}

		STUDENT x;

		token = strtok(buff, " ");
		strcpy(x.firstName, token);

		token = strtok(NULL, " ");
		strcpy(x.lastName, token);

		token = strtok(NULL, " ");
		x.ceg = atof(token);

		token = strtok(NULL, " ");
		x.cs = atof(token);

		table[i] = x;
  }

  printTable(table);
  return 0;
}
