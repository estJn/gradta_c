#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

/* Display the manuscript on the screen. */
void manuscript(){
  printf("\nMANUSCRIPT:\n");
  printf("start – start a new game.\n");
  printf("add – place more money in user’s wallet.\n");
  printf("print [options] (e.g. print history)\n");
  printf("\t options: \n");
  printf("\t menu – display a manuscript\n");
  printf("\t bet – display all the inside bet type\n");
  printf("\t history – display the user’s play history\n");
  printf("exit – display the user’s betting HISTORY and exit the program.\n");
}//print manuscript()

void printBet(){
  printf("\nTYPE\t\tDESCRIPTION\t\t\t\t\t\tPAYOUT\n");
  printf("straightup\tBet on a single number\t\t\t\t\t35:1\n");
  printf("split\t\tBet on two numbers which are adjacent on the table\t17:1\n");
  printf("square\t\tBet on four numbers which are adjacent to each other\t8:1\n");
  printf("street\t\tBet on three consecutive numbers in a same row\t\t11:1\n");
  printf("line\t\tBet on six consecutive numbers (two street)\t\t5:1\n");
  printf("trio\t\tBet on 0, 1, and 2 OR 0, 2, and 3\t\t\t11:1\n");
  printf("firstfour\tBet on 0, 1, 2, and 3\t\t\t\t\t6:1\n");
}

void printHistory(char HISTORY[][6][32], int count){

  printf("\nPLAY HISTORY:\nBet Type\tBet Number(s)\t\tBet $\t\t\tBall #\tResult\tBalance\n");
  int row;
  for(row = 0; row < count; row++){
        printf("%-15s %-23s %-15s\t\t%s\t%s\t%s\n", HISTORY[row][0], HISTORY[row][1], HISTORY[row][2], HISTORY[row][3], HISTORY[row][4], HISTORY[row][5]);
  }
  printf("\nEnd of History Reports\n");
}//printHistory()

void record(char HISTORY[][6][32], char *TYPE, int NUM[], double AMOUNT, int BALL, double RESULT, double *WALLET, int counter, int nTokenSz){

  *WALLET += RESULT;
  double balance = *WALLET + RESULT;
  char numStr[32], amountStr[32], ballStr[32], balanceStr[32];

  //Convert Number (array, double, int) to a String.
  int k=0, i=0;
  for(; i < nTokenSz; i++){
    sprintf(numStr+k, "%d, ", NUM[i]);
    k+=3;
  }
  sprintf(amountStr, "%.2lf", AMOUNT);
  sprintf(ballStr, "%d", BALL);
  sprintf(balanceStr, "%.2lf", *WALLET);

  // Store Data into the HISTORY 3D array.
  strcpy(HISTORY[counter][0], TYPE);
  strcpy(HISTORY[counter][1], numStr);
  strcpy(HISTORY[counter][2], amountStr);
  strcpy(HISTORY[counter][3], ballStr);
  strcpy(HISTORY[counter][5], balanceStr);

  if(RESULT > 0){
    strcpy(HISTORY[counter][4], "WIN");
  }else{
    strcpy(HISTORY[counter][4], "LOST");
  }
}

int Smallest(int list[], int N, int cI){

  // largest value and its index at the list array.
  int minValue = list[cI];
  int mIndex = cI;

  //Iterate the list
  int k;
  for(k = cI + 1; k < N; k++){
    // if the current comparison value is larer than the previous largest value
    // update the max_Value and the index.
    if(list[k] < minValue){
      minValue = list[k];
      mIndex = k;
    }
  }
  // return the index of the largest value in the list from m_CurrentIndex to N.
  return mIndex;
}

// sorting from Lab 4 except its in ascending order now!
void sort(int list[], int N){
  int minIndex; // j-th index where the largest value is sotred in the list
  int tmp; // temporary value holder.

  //sort
  int i;
  for(i = 0; i < N-1; i++){
    // get the index of the largest value in the list from k to N-1
    minIndex = Smallest(list, N, i);

    // switch the current value list[k_currentIndex] and list[j_MaxIndex]
    tmp = list[i];
    list[i] = list[minIndex];
    list[minIndex] = tmp;
  }
}

double CALCULATE(int BALL, int NUM[], double AMOUNT, int PAYOUT, int nTokenSz){

  int luck = 0;
  double result = 0;

  int i;
  for(i = 0; i < nTokenSz; i++){
    if(NUM[i] == BALL){
      luck = 1;
    }
  }

  if(luck){
    result = AMOUNT * PAYOUT;
  }else{
    result -= AMOUNT;
  }

  return result;
}//CALCULATE()

int arraycmp(int a[], int b[], int n){

  int i;
  for(i = 0; i < n; i++){
    if(a[i] != b[i]){
      return 0;
    }
  }
  return 1;
}//arraycmp()

int validation(int type, int num[]){

  int valid = 0;

  int v = 0;
  for(; v < sizeof(num); v++){
    if(num[v] < 0 || num[v] > 36){
      return 0;
    }
  }

  int choiceOne[] = {0, 1, 2};
  int choiceTwo[] = {0, 2, 3};
  int numChoice[] = {0, 1, 2, 3};

  switch(type){
    case 2: // split
      if((num[0] % 3) != 0){
        // {1,2} {2,3} {4,5} {5,6} {7,8} ... {34,35} {35,36}
        // OR
        // {1,4} {2,5} {4,7} {5,8} {7,11} ... {31,34} {32,35}
        valid = (num[1] == num[0] + 1) || (num[1] == num[0]+3);
      }else{
        // {3,6} {6,9} ... {30,33} {33,36}
        valid = (num[1] == num[0]+3);
      }
    break;

    case 3: // street
      // {1,2,3} {4,5,6} {7,8,9} {10,11,12} ... {31,32,33} {34,35,36}
      valid = ((num[0] % 3) == 1 && (num[1] == num[0]+1) && (num[2] == num[0]+2));
    break;

    case 4: // square
      // {1,2,4,5} {2,3,5,6} {4,5,7,8} {5,6,8,9} ... {31,32,34,35} {32,33,35,36}
      valid = ((num[0] % 3) != 0 && (num[1] == num[0]+1) && (num[2] == num[0]+3) && (num[3] == num[0]+4));
    break;

    case 6: // line
      // two consecutive street
      valid = ((num[0] % 3) == 1 && (num[1] == num[0]+1) && (num[2] == num[0]+2)); // first street
      valid = valid && ((num[3] == num[0]+3) && (num[4] == num[0]+4) && (num[5] == num[0]+5)); // second consecutive street
    break;

    case 20: // trio
      // {0, 1, 2} OR {0, 2, 3}
      valid = arraycmp(num, choiceOne, 3) || arraycmp(num, choiceTwo, 3);
    break;

    case 30: // firstfour
      // {0, 1, 2, 3}
      valid = arraycmp(num, numChoice, 4);
    break;

    default:
      valid = 0;
    break;
  }
  return valid;
}

int ROULETTE(double *WALLET, char HISTORY[][6][32], int historyCounter){

  //Randomly Generated Number where the Roulette Ball Landed
  srand(time(NULL));
  int BALL = rand() % 37;

  char BET[1024], *TYPE;
  int PAYOUT, NUM[6];
  double AMOUNT;

  int nTokenSz = 0;
  char *token, *numtoken[16]={NULL};

  int invalid = 0, doCal = 0;

  do{
    printf("\nBET Usage: <Betting Type> <Betting Number(s)> <Betting Amount>\n");
    printf("Type \"no\" to leave the game and take a new action.\n\n");
    printf("BET: ");
    fgets(BET, sizeof(BET), stdin);

    invalid = 0;

    size_t strLength = strlen(BET)-1;
    if(BET[strLength] == '\n'){
      BET[strLength] = '\0';
    }

    if(!strcmp(BET, "no")){
      return 0;
    }
    else{

      // Get Betting Type
      token = strtok(BET, " ");
      TYPE = token;

      token = strtok(NULL, " ");

      // Get Betting Number & Amount in a string
      nTokenSz = 0;
    	while(token != NULL && atoi(token) >= 0){
    		numtoken[nTokenSz++] = token;
    		token = strtok(NULL, " ");
    	}

      if(nTokenSz > 1){
        // Store numbers in an integer array
        int i;
        for(i = 0; i < nTokenSz-1; i++){
          char* x = numtoken[i];
          int tmpI = atoi(x);
          if(tmpI >= 0 && tmpI <= 36){
            NUM[i] = tmpI;
          }
        }
        //sort numbers
        sort(NUM, nTokenSz-1);
        // Amount as a double type variable
        AMOUNT = atof(numtoken[nTokenSz-1]);
        nTokenSz--;
      }//IF(nTokenSz > 1)

      if(!strcmp(TYPE, "straightup") && nTokenSz == 1){
        PAYOUT = 35;
        doCal = 1;
      }
      else if(!strcmp(TYPE, "split") && nTokenSz == 2){
        doCal = validation(2, NUM);
        PAYOUT = 17;
      }
      else if(!strcmp(TYPE, "street") && nTokenSz == 3){
        doCal = validation(3, NUM);
        PAYOUT = 11;
      }
      else if(!strcmp(TYPE, "square") && nTokenSz == 4){
        doCal = validation(4, NUM);
        PAYOUT = 8;
      }
      else if(!strcmp(TYPE, "line") && nTokenSz == 6){
        doCal = validation(6, NUM);
        PAYOUT = 5;
      }
      else if(!strcmp(TYPE, "trio") && nTokenSz == 3){
        doCal = validation(20, NUM);
        PAYOUT = 11;
      }
      else if(!strcmp(TYPE, "firstfour") && nTokenSz == 4){
        doCal = validation(30, NUM);
        PAYOUT = 6;
      }
      else{
        invalid = 1;
        printf("ERROR:\n\tInvalid betting format.\n\tRemember the program is case sensitive.\n");
      }
    }//if NO-BET, else BET

    if(doCal == 0){
      invalid = 1;
      printf("ERROR:\n\tInvalid betting number (numbers range: 0 - 36).\n\tTake 'print bet' action to read details on your betting choices.\n");
    }
  }while(invalid);

  if(AMOUNT <= *WALLET){
    double result = CALCULATE(BALL, NUM, AMOUNT, PAYOUT, nTokenSz);

    printf("Roulette Number: %d \n", BALL);
    if(result > 0){
      printf("YOU WIN! (%.2lf) Balance: %.2lf\n", result, *WALLET + result);
    }else{
      printf("YOU LOSE: try it again!\n Before: %.2lf (%.2lf) New Balance: %.2lf\n", *WALLET, result, *WALLET + result);
    }

  record(HISTORY, TYPE, NUM, AMOUNT, BALL, result, WALLET, historyCounter, nTokenSz);

  }else{
    printf("ERROR:\n\tYour balance is lower than what you bet.\nTake 'add' action to add more money!\n");
  }//(bet $ <= wallet)

  return 1;
}// ROULETTE()

int lounge( double *WALLET, char HISTORY[][6][32]){

  manuscript();

  char buffer[32], ACTION[32];
  int play = 0;

  // prompt for an action.
  printf("Take an action from manuscript! (e.g. start)\n\nAction: ");
  fgets(ACTION, sizeof(ACTION), stdin);

  size_t strLength = strlen(ACTION)-1;
  if(ACTION[strLength] == '\n'){ ACTION[strLength] = '\0'; }

  while(strcmp(ACTION, "exit")){
    if(!strcmp(ACTION, "start")){
      if(WALLET > 0){
        printf("Start a NEW game!\n");
        play += ROULETTE(WALLET, HISTORY, play);
      }else{
        printf("ERROR:\n\tNot Enough money to start the game!\nTake 'add' action do add more amount on your balance.\n");
      }
    }
    else if(!strcmp(ACTION, "add")){
      // Add more $ in the WALLET balance.
      double addAmount;
      printf("Current Balance: %.2lf\n", *WALLET);
      printf("Type an amount you want to add on your balance\n");
      printf("Add Amount: ");
      fgets(buffer, sizeof(buffer), stdin);
      addAmount = atof(buffer);

      *WALLET += addAmount;
      printf("\nNew Balance: %.2lf\n\n", *WALLET);
    }
    else if(!strcmp(ACTION, "print menu")){
      manuscript();
    }
    else if(!strcmp(ACTION, "print bet")){
      printBet();
    }
    else if(!strcmp(ACTION, "print history")){
      printHistory(HISTORY, play);
    }else{
      // Invalid keyboard ACTION
      printf("Unable to recognize the \"%s\" action.\n", ACTION);
      printf("Note: the program is Case Sensitive.\n");
      manuscript();
    }//if(strcmp)

    printf("\n----------------------------------\n");
    printf("Take an other Action: ");
    fgets(ACTION, sizeof(ACTION), stdin);
    size_t strLength = strlen(ACTION)-1;
    if(ACTION[strLength] == '\n'){ ACTION[strLength] = '\0'; }

  }//while( not "exit"), run ACTION

  return play;
}//lounge()

int main(){

  //String sizes
  char buffer[32];
  double WALLET;
  double *wptr;
  char HISTORY[100][6][32];

  printf("\nWelcome to the mini roulette simulation game.\n\n");

  // Prompt Initial Amount
  printf("Enter Initial Amount (1 - 10,000) to start the game\nAmount: $");
  fgets(buffer, sizeof(buffer), stdin);
  WALLET = atof(buffer);

  //scanf("%lf", &WALLET);

  while(WALLET < 1 || WALLET > 10000){
      printf("Initial Amount should be between 1 and 10,000.\n");
      printf("Please Re-enter the Initial Amount (1 - 10,000)");
      fgets(buffer, sizeof(buffer), stdin);
      WALLET = atof(buffer);
  }

  wptr = &WALLET;
  int count = lounge(wptr, HISTORY);
  printHistory(HISTORY, count);
  printf("EXIT the Roulette Simulation Game.\n\n");

  return 0;
}
