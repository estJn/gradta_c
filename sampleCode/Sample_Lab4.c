#include <stdio.h>

/* PRINT THE List of size N*/
void printLst(int list[], int N){
  printf("\t\t{ ");
  for(int i = 0; i < N; i++){
    printf("%d ", list[i]);
  }
  printf("}\n");
}

/* Find the index of the Largest value from m_CurrentIndex to N in the list
  Then RETURN it */
int Largest(int list[], int N, int m_CurrentIndex){

  // largest value and its index at the list array.
  int L_MaxValue = list[m_CurrentIndex];
  int j_MaxIndex = m_CurrentIndex;

  //Iterate the list
  for(int k = m_CurrentIndex + 1; k < N; k++){
    // if the current comparison value is larer than the previous largest value
    // update the max_Value and the index.
    if(list[k] > L_MaxValue){
      L_MaxValue = list[k];
      j_MaxIndex = k;
    }
  }

  // return the index of the largest value in the list from m_CurrentIndex to N.
  return j_MaxIndex;
}

/* For given integer list with a size of N, sort the list in descending order.*/
void Sort(int list[], int N){
  int j_MaxIndex; // j-th index where the largest value is sotred in the list
  int tmp; // temporary value holder.

  //sort
  for(int k_CurrentIndex = 0; k_CurrentIndex < N-2; k_CurrentIndex++){
    // get the index of the largest value in the list from k to N-1
    j_MaxIndex = Largest(list, N, k_CurrentIndex);

    // switch the current value list[k_currentIndex] and list[j_MaxIndex]
    tmp = list[k_CurrentIndex];
    list[k_CurrentIndex] = list[j_MaxIndex];
    list[j_MaxIndex] = tmp;

    // print the list if there is a change in the list.
    if(j_MaxIndex != k_CurrentIndex){
      printLst(list, N);
    }
  }
}

int main(){

  //program description
  printf("This progarm will take N (0 < N <= 100) integers and sort them in descending order.\n\n");

  int N, valid = 0;
  int list[100];

  while(!valid){
    // prompt the user for N
    printf("Type the total number of integers to sort (N): ");
    scanf("%d", &N);

    // if-else: N value validation check
    if(N > 0 && N <= 100){
      valid = 1; // no need to ask for N and the list numbers again!

      // prompt the user for N integers
      printf("Type %d integers: ", N);
      for(int i = 0; i < N; i++){
          scanf("%d", &list[i]);
      }
      // Before Sort
      printLst(list,N);

      Sort(list, N);

    }else{
      // Invalid input N
      printf("Invalid Input. N should be greater than 0 and less than 101.\n");
      valid = 0;
    }
  }


  return 0;
}
