#include <stdio.h>

int main(){

  double principal = 0.0;
  int year = 0, ratePercent = 0;

  //Prompt User for keyboard input
  printf("\nThe following program calculate the compounded interest and \n\tprints out the saving at the end of each year.\n\nEnter following,\n");
  printf("\tInitial Principal: ");
  scanf("%lf", &principal);
  printf("\tAnnual Interest Rate(%%): ");
  scanf("%lf", &ratePercent);
  printf("\tNumber of Years: ");
  scanf("%d", &numYear);
  printf("\n");

  //loop counter
  int yearCounter = 1;
  //rate in double
  double rate = (double) ratePercent/100;

  while(yearCounter <= year){
    principal += (principal* rate);
    printf("after %d year: %.2lf\n", yearCounter, principal);
    yearCounter++;
  }

  printf("\n");
  return 0;
}
