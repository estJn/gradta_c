#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct profile{
  char first[32];
  char last[32];
  char sex[32];
  char occupation[32];
  char hair[32];
  char eye[32];
  char other[32];
  int age;
  double height;
}Profile;

typedef struct node{
  Profile p;
  struct node *next;
}Node;

typedef struct comparison{
  char age;
  char height;
}Comparison;
/*
  PRE-CONDITION:
    the file must be opened
  FUNCTION:
  POST-CONDITION:

*/

void printSuspect(Node * root){

  Node *ptr = root;

  printf("\nSUSPECT DESCRIPTION:\n");
  while(ptr != NULL){
    printf("%s\t%7s\t(%5d)\t%s\t%s\t%13.2lf\t%s\t%s\t%s\n", ptr->p.last, ptr->p.first, ptr->p.age, ptr->p.sex, ptr->p.occupation, ptr->p.height, ptr->p.hair, ptr->p.eye, ptr->p.other);
    ptr = ptr->next;
  }
  printf("END DESCRIPTION\n");
}
void printRobber(Profile * r, Comparison *cmp){

  printf("\nROBBER DESCRIPTION:\n");
  if(*(r->sex) != 0){
    printf("%s\n", r->sex);
  }
  if(r->age != 0){
    printf("Age: %c %d\n", cmp->age, r->age);
  }
  if(r->height != 0.0){
    printf("Height: %c %.2lfcm\n", cmp->height, r->height);
  }
  if(*(r->occupation) != 0){
    printf("Occupation: %s\n", r->occupation);
  }
  if(*(r->hair) != 0){
    printf("Hair Color: %s\n", r->hair);
  }
  if(*(r->eye) != 0){
    printf("Eye Color: %s\n", r->eye);
  }
  if(*(r->other) != 0){
    printf("Other Description: %s\n", r->other);
  }

  printf("END DESCRIPTION\n");
}

Node* addSort(Node *root, Node *new){

  if(strcmp(new->p.last, root->p.last) < 0){
  // IF the new_node Lastname < first_node Lastname
  // Add new_node in front of the existing linked-list
    new->next = root;
    root = new;

  }else{

    Node *previous = root;
    Node *current = root->next;

    while(current != NULL){

      if(strcmp(new->p.last, current->p.last) < 0){
        // IF the previous_node Lastname <= new_node Lastname < current_node Lastname
        // Add new_node between the previous & current node.
        new->next = current;
        previous->next = new;
        return root;
      }
      previous = current;
      current = current->next;
    }

    // Current node is End of the list NULL. No more node to compare!!
    // Add new_node at the end of the existing linked-list
    previous->next = new;

  }//If root Else middle/end
  return root;
}

Node* suspect(FILE *f){
  char line[1024];
  Node *first = NULL;
  Node *last = NULL;
  int count = 0;
  // Read the file line by line while creating a new linked-list node
  // Tokenize the line and store each corresponding data to the node.
  // Then, assign the node as the first linked node OR append it to the current existing linked-list.
  while(fgets(line, sizeof(line), f)){
    if(line[strlen(line) -1] == '\n'){ line[strlen(line)-1] = '\0'; }

    Node *new = (Node *) malloc(sizeof(Node));

    strcpy(new->p.first, strtok(line, " "));
    strcpy(new->p.last, strtok(NULL, " "));
    strcpy(new->p.sex, strtok(NULL, " "));
    new->p.age = atoi(strtok(NULL, " "));
    strcpy(new->p.occupation, strtok(NULL, " "));
    new->p.height = atof(strtok(NULL, " "));
    strcpy(new->p.hair, strtok(NULL, " "));
    strcpy(new->p.eye, strtok(NULL, " "));
    strcpy(new->p.other, strtok(NULL, " \r"));
    new->next = NULL;

    if(first == NULL){
      // first node created
      first = new;
      last = new;
    }else{

      first = addSort(first, new);

    }// IF FIRST ELSE NOT

  }// WHILE (LINE)

  return first;
}

int robber(FILE *f, Profile *r, Comparison *cmp){
  char line[1024];
  int lineNum;

  while(fgets(line, sizeof(line), f) && strcmp(line, "\n")){

    if(line[strlen(line) -1] == '\n'){ line[strlen(line)-1] = '\0'; }

    char*type = strtok(line, ": ");
    char*description = strtok(NULL, " \r");

    if(!strcmp(type, "sex")){
      //r->sex = malloc(strlen(description)+1);
      strcpy(r->sex, description);
    }
    else if(!strcmp(type, "occupation")){
      //r->occupation = malloc(strlen(description)+1);
      strcpy(r->occupation, description);
    }
    else if(!strcmp(type, "hair-color")){
      //r->hair = malloc(strlen(description)+1);
      strcpy(r->hair, description);
    }
    else if(!strcmp(type, "eye-color")){
      //r->eye = malloc(strlen(description)+1);
      strcpy(r->eye, description);
    }
    else if(!strcmp(type, "other")){
      //r->other = malloc(strlen(description)+1);
      strcpy(r->other, description);
    }
    else if(!strcmp(type, "age")){
      cmp->age = description[0];
      r->age = atoi(strtok(NULL, " \r"));
    }
    else if(!strcmp(type, "height")){
      cmp->height = description[0];
      r->height = atof(strtok(NULL, " \r"));
    }
    lineNum++;
  }//WHILE(LINE)

  return lineNum;
}

void findTheif(Node *root, Profile *r, Comparison *cmp, int nDescription){

  printf("\n ----FOUND the ROBBER!!!----\n");

  Node *suspect = root;
  while(suspect != NULL){
  int match = 0;
    if(!strcmp(r->sex, suspect->p.sex)){
      match++;
    }
    if(!strcmp(r->occupation, suspect->p.occupation)){
      match++;
    }
    if(!strcmp(r->hair, suspect->p.hair)){
      match++;
    }
    if(!strcmp(r->eye, suspect->p.eye)){
      match++;
    }
    if(!strcmp(r->other, suspect->p.other)){
      match++;
    }
    if(r->age != 0){
      if(cmp->age == '<' && (r->age > suspect->p.age)){
        match++;
      }else if(cmp->age == '=' && (r->age == suspect->p.age)){
        match++;
      }else if(cmp->age == '>' && (r->age < suspect->p.age)){
        match++;
      }
    }
    if(r->height != 0){
      if(cmp->height == '<' && (suspect->p.height < r->height)){
        match++;
      }else if(cmp->height == '=' && (r->height == suspect->p.height)){
        match++;
      }else if(cmp->height == '>' && (suspect->p.height > r->height)){
        match++;
      }
    }

    if(match == nDescription){
      printf(" Name: %s %s\n", suspect->p.first, suspect->p.last);
      printf(" %dold %s\n Occupation: %s\n Height: %.2lfcm\n", suspect->p.age, suspect->p.sex, suspect->p.occupation, suspect->p.height);
      printf(" Hair-Color: %s\n Eye-Color: %s\n Other: %s\n", suspect->p.hair, suspect->p.eye, suspect->p.other);
    }

    suspect = suspect->next;
  }
  printf("\n ---------------------------\n");
}

int main(){

  // prompt the file name.
  char fname[32];
  printf("Type Suspect File Name: ");
  fgets(fname, sizeof(fname), stdin);
  if(fname[strlen(fname) -1] == '\n'){ fname[strlen(fname)-1] = '\0'; }

  // open file
  FILE *f = fopen(fname, "r");

  // if the file cannot be opened, print an error message and re-prompt
  // until the user types a filename which can be opened.
  while(f == NULL){
    printf("File I/O Error: unable to open the filename '%s'.\n\n", fname);
    printf("Type Correct File Name: ");
    fgets(fname, sizeof(fname), stdin);
    if(fname[strlen(fname) -1] == '\n'){ fname[strlen(fname)-1] = '\0'; }
    f = fopen(fname, "r");
  }

  Node * root = suspect(f);
  fclose(f);

  printf("Type Robber Description File Name: ");
  fgets(fname, sizeof(fname), stdin);
  if(fname[strlen(fname) -1] == '\n'){ fname[strlen(fname)-1] = '\0'; }
  // open file
  FILE *fp = fopen(fname, "r");

  // if the file cannot be opened, print an error message and re-prompt
  // until the user types a filename which can be opened.
  while(fp == NULL){
    printf("File I/O Error: unable to open the filename '%s'.\n\n", fname);
    printf("Type Correct File Name: ");
    fgets(fname, sizeof(fname), stdin);
    if(fname[strlen(fname) -2] == '\r'){ fname[strlen(fname)-1] = '\0'; }
    else if(fname[strlen(fname) -1] == '\n'){ fname[strlen(fname)-1] = '\0'; }
    fp = fopen(fname, "r");
  }

  Profile theif = {0, 0, 0, 0, 0, 0, 0};
  Comparison cmp = {.age = '-', .height = '-'};
  int n = robber(fp, &theif, &cmp);
  fclose(fp);

  printSuspect(root);
  printRobber(&theif, &cmp);
  findTheif(root, &theif, &cmp, n);

  return 0;
}
