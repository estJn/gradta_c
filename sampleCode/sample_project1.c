#include <stdio.h>

double LOG[10][3]; //global variable

void printLog(int count){

  //label
  printf("\nEmployee Donations in year 2017:\nNO\tSALARY\t\tDEDUCTION\tNET\n");

  for(int row = 0; row < count; row++){
    printf("%d\t%.1lf\t\t%.3lf\t\t%.3lf\n", row + 1, LOG[row][0], LOG[row][1], LOG[row][2]);
  }
  printf("End of the Report.\n\n");
}
void storeLog(double salary, double deduction, double net, int count){
  // Store data in the 2D array LOG.
  LOG[count][0] = salary;
  LOG[count][1] = deduction;
  LOG[count][2] = net;
}

double Salary(int min, int max){

  double salary, valid = 0;
  while(!valid){

    printf("Enter SALARY (%d - %d): ", min, max);
    scanf("%lf", &salary);

    if(salary < min || salary > max){
      valid = 0;
      printf("Salary is Out of Range.\n");
    }else{
      valid = 1;
    }

  }//while(valid)

  return salary;
}//Salary()

void printMenu(){
  printf("\nSalary ranges and the percent deduction for year 2017:\n");
  printf("ID\t Salary\t\t Deduction Rate\n");
  printf("1\t100-1000\t 0.50\%\n");
  printf("2\t1001-5000\t 1.50\%\n");
  printf("3\t5001-10000\t 2.50\%\n");
  printf("4\t10001-15000\t 3.50\%\n");
  printf("5\t15001-20000\t 4.50\%\n");
}

int main(){

  int ID;
  double SALARY, DEDUCTION, NET;

  int quit = 0, count = 0;;

  printMenu();

  while(!quit && count < 10){

    //prompt user for an ID
    printf("\nEnter ID (or 0 to exit the program): ");
    scanf("%d", &ID);

    switch (ID) {
      case 0: //Exit the program
        printLog(count);
        quit = 1;
        break;

      case 1:
        SALARY = Salary(100, 1000);
        DEDUCTION = SALARY * (0.50/100);
        NET = SALARY - DEDUCTION;
        printf("Base Salary: $%.2lf, Deduction Amount: $%.2lf, Net Salary: $%.2lf\n", SALARY, DEDUCTION, NET);
        storeLog(SALARY, DEDUCTION, NET, count);
        count++;
        break;

      case 2:
        SALARY = Salary(1001, 5000);
        DEDUCTION = SALARY * (1.50/100);
        NET = SALARY - DEDUCTION;
        printf("Base Salary: $%.2lf, Deduction Amount: $%.2lf, Net Salary: $%.2lf\n", SALARY, DEDUCTION, NET);
        storeLog(SALARY, DEDUCTION, NET, count);
        count++;
        break;

      case 3:
        SALARY = Salary(5001, 10000);
        DEDUCTION = SALARY * (2.50/100);
        NET = SALARY - DEDUCTION;
        printf("Base Salary: $%.2lf, Deduction Amount: $%.2lf, Net Salary: $%.2lf\n", SALARY, DEDUCTION, NET);
        storeLog(SALARY, DEDUCTION, NET, count);
        count++;
        break;

      case 4:
        SALARY = Salary(10001, 15000);
        DEDUCTION = SALARY * (3.50/100);
        NET = SALARY - DEDUCTION;
        printf("Base Salary: $%.2lf, Deduction Amount: $%.2lf, Net Salary: $%.2lf\n", SALARY, DEDUCTION, NET);
        storeLog(SALARY, DEDUCTION, NET, count);
        count++;
        break;

      case 5:
        SALARY = Salary(15001, 20000);
        DEDUCTION = SALARY * (4.50/100);
        NET = SALARY - DEDUCTION;
        printf("Base Salary: $%.2lf, Deduction Amount: $%.2lf, Net Salary: $%.2lf\n", SALARY, DEDUCTION, NET);
        storeLog(SALARY, DEDUCTION, NET, count);
        count++;
        break;

      default:
        printf("Invalid ID. ID must be an integer between 1 and 5 (or 0 to exit the program).\n");
        break;
    }//switch(ID)
  }//while(quit or count < 10)

  if(count == 10){
    printLog(count);
  }

  return 0;
}
