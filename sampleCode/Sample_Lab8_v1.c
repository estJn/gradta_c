#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(){

  // Prompt for a FileName
  char filename[32];
  printf("Enter Filename: ");
  fgets(filename, sizeof(filename), stdin);
  if(filename[strlen(filename)-1] == '\n'){ filename[strlen(filename)-1] = '\0'; }

  char saveBuff[4096]; // temporary buffer/string which holds the data in the file

  // Set File Exist flag
  int existFlag = 1;
  FILE *fr = fopen(filename, "r"); // Open a file with a read mode -readFile
  if(fr == NULL){
    existFlag = 0;
    printf("Creating file: %s\n", filename); // File NOT Exists
  }else{
    printf("Opening file: %s\n", filename); // File Exists

    /* Read the file line by line using fgets */
    char rline[2048];

    strcpy(saveBuff, ""); // initialized the temporary buffer

    printf("\n------- Start Reading %s -------\n", filename);
    // Display Existing File Data on the screen
    while(fgets(rline, sizeof(rline), fr)){
      printf("%s", rline); // Deisplay data in the file on the screen
      strcat(saveBuff, rline); // Added the currently read line into the temporary saveBuffer
    }
    printf("\n------- End Reading %s -------\n", filename);
    fclose(fr); // close readFile
  }

  // Prompt for a Command: Write w or Append a
  char cmd[8];
  printf("\nCommand:\n\t'w' to write from top of file\n\t'a' to write to the bottom of file\n%s command: ", filename);
  fgets(cmd, sizeof(cmd), stdin);
  if(cmd[strlen(cmd)-1] == '\n'){cmd[strlen(cmd)-1] = '\0';}

  // Invalid command cmd Handling!
  while(strcmp(cmd, "w") && strcmp(cmd, "a")){

    // Invalid Command Error Message
    printf("Error: Invaild Input. Command must be w or q\n\n");
    printf("\nCommand:\n\t'w' to write from top of file\n\t'a' to write to the bottom of file\n%s command: ", filename);

    //Get New command cmd input
    fgets(cmd, sizeof(cmd), stdin);
    if(cmd[strlen(cmd)-1] == '\n'){cmd[strlen(cmd)-1] = '\0';}
  }

  /* Open File to read/write/append */
  // Write a File
  if(!strcmp(cmd, "w")){
    FILE *fw = fopen(filename, "w+");//open a file with an open+ mode -WriteFile

    char wline[2048];
    printf("Start Writing from Top of the File%s\n\n", filename);

    fgets(wline, sizeof(wline), stdin); // Get 1st Keyboard Input

    /* Over-write the existing file */
    /* WHILE user doesnt type newline */
    while(strcmp(wline, "\n")){
      fprintf(fw, "%s",wline); // Write into a File
      fgets(wline, sizeof(wline), stdin); // Get Keyboard Input
    }

    fprintf(fw, "%s", saveBuff); // append saved Data

    printf("End Write 'w'\n");
    fclose(fw);//close WriteFile
  }

  // Append a File
  if(!strcmp(cmd, "a")){
    FILE *fa = fopen(filename, "a+");//open a file with an append+ mode -appendFile
    char aline[2048];
    printf("Start Writing from Bottom of the File%s\n\n", filename);

    fgets(aline, sizeof(aline), stdin); // Get 1st Keyboard Input

    /* Do NOT over-write the existing file; write after the end of existing data */
    /* WHILE user doesnt type newline */
    while(strcmp(aline, "\n")){
      fprintf(fa, "%s",aline); // Apppend into a File
      fgets(aline, sizeof(aline), stdin); // Get Keyboard Input
    }

    printf("End Append 'a'\n");
    fclose(fa);//close appendFile
  }

  printf("\nExit the program.\n\n");

  return 0;
}
