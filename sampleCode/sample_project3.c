#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <ctype.h>

/*
  STRUCT:
    construct a new struct & type called CODE, holding
      a single character and an integer.
*/
typedef struct Code {
  char ch;
  int num;
} CODE;


/* FUNCTION: display the manuscirpt of the commands */
void usage(){
  printf("USAGE:\n");
  printf("Command to encrypt a sentence to a numeric code:\n");
  printf("\tencrypt <sentence>\tOR\te <your_sentence>\n");
  printf("Command to decrypt a numeric code to a readable sentence:\n");
  printf("\tdecrypt <number_code>\tOR\td <your_number_code>\n");
  printf("To exit the program, type 'quit' or 'q'.\n");
}

/*
  PRE-CONDITION:
    a struct CODE array and the correct max-number indexed counter
  FUNCTION:
    display the elements in the array
  POST-CONDITION:
    displayed conversion code list.
*/
void printArray(CODE a[], int *count){

  int i;
  printf("Char Number\n");
  /* Display each indexed elements from the CODE array */
  for(i = 0; i < *count; i++){
    printf("%c\t%d\n", a[i].ch, a[i].num);
  }
  printf("END");

}//printList()

/*
  PRE-CONDITION:
    the file must be opened!
  FUNCTION:
    Read the opened file line by line and Store the line data into the CODE array
    Each line has a single character folled by a two digits number;
      character is stored at the struct CODE's character
      and numbers is stored at the struct CODE's integer
  POST-CONDITION:
    the struct CODE array is modified/updated and the counter is incremented!

*/
int createList(FILE * f, CODE *code, int *count){

  // Extract data from the file line by line
  char line[128];
  while(fgets(line, sizeof(line), f)){

    // store data
    code[*count].ch = line[0];
    code[*count].num = atoi(line+1);

    // increment the array index counter
    *count += 1;
    printf("%d, ", *count);
  }// WHILE (LINE)

  return 0;
}

/*
  PRE-CONDITION:
    correct converion codes in CODE struct, conversion code counter, and a character
  FUNCITON:
    Using for-loop to go throw each elements in the CODE array.
    If the element has the matching character we are looking for,
      program stop and return element's integer to the caller
    Else keep look for the element with the matching character.
  RETURN:
    If the character is found in the conversion code array,
      return the corresponding two digits integers.
    ELSE, return 0;
*/
int getNum(CODE *code, int *count, char x){

  int i;
  for(i = 0; i < *count; i++){
    if(code[i].ch == x){
      return code[i].num;
    }
  }
  return 0;
}
/*
  PRE-CONDITION:
    correct converion codes in CODE struct, conversion code counter, and two digit integer
  FUNCITON:
    Using for-loop to go throw each elements in the CODE array.
    If the element has the matching integer we are looking for,
      program stop and return element's character to the caller
    Else keep look for the element with the matching integer.
  RETURN:
    If the number is found in the conversion code array,
      return the corresponding character
    ELSE, return 0;
*/
char getChar(CODE *code, int *count, int x){
  int i;
  for(i = 0; i < *count; i++){
    if(code[i].num == x){
      return code[i].ch;
    }
  }
  return 0;
}

/*
  PRE-CONDITION:
    correct converion codes in CODE struct, conversion code counter, and a sentence to encode
  FUNCITON:
    Loop the sentence to get a character one by one
      and fidn its matching numeric code by calling getNum function
    If the numeric code exists, keep convert it and print out the result -encoded text
    Otherwise, display an error message and encoded failed.
  POST-CONDITION:
    If all the character has a unique conversion Code, display encoded sentence and reutrn 1;
    Else, print out an error message and return 0.
  RETURN:
    1 if successfully encoded, else 0.
*/
int encryption(CODE *code, int *count, char* s){

  int i, eCode;
  char tmp[4], str[2048] = "";
  // go throw each character in the sentence
  for(i = 0; *(s+i) != '\0' ; i++){
    // call getNum function to find the conversin code element and the matching number
    eCode = getNum(code, count, s[i]);
    if(eCode){
      // number exist; convert the integer to a temporary string.
      sprintf(tmp, "%d", eCode);
    }else{
      // conversion code does not exist.
      printf("Encryption Error: Detected Invalid Character '%c'.\n\n", s[i]);
      return 0;
    }
    // add the temporary convered number string into the result string.
    strcat(str, tmp);
  }
  // display correct encoded text.
  printf("Encrypt Code: %s\n\n", str);
  return 1;
}//Encryption()
/*
  PRE-CONDITION:
    correct converion codes in CODE struct, conversion code counter, and a sentence to decode
  FUNCITON:
    Loop the sentence to two digit numbers (read two character at once)
      and fidn its matching character code by calling getChar function.
    If the character code exists, keep convert it and print out the result -decoded text
    Otherwise, display an error message and decoded failed.
  POST-CONDITION:
    If all the two digits integer has a unique conversion Code, display decoded sentence and reutrn 1;
    Else (not integers, or unable to detect the conversion code...), print out an error message and return 0.
  RETURN:
    1 if successfully encoded, else 0.
*/
int decryption(CODE *code, int *count, char* s){

  int num;
  char dCode, tmp[4], str[2048] = "";

  int i = 0;
  // go thrwo the sentence; read two character at a time
  while(*(s+i) != '\0'){

    //IF two digit numeric code
    if(isdigit(s[i]) && isdigit(s[i+1])){
      // Two digit String to nubmer
      num = (s[i] - '0') * 10 + (s[i+1] - '0');
      // call getChar function to find the conversin code element and the matching character
      dCode = getChar(code, count, num);
      if(dCode){
        // character exist; convert the character to a temporary string.
        sprintf(tmp, "%c", dCode);
      }else{
        // conversion code does not exist.
        printf("Decryption Error: Detected Invalid Numeric Code '%i'.\n\n", num);
        return 0;
      }

        //increment the location by 2.
       i+=2;
     }else{
       // non-numeric code.
       printf("Decryption Error: Detected Invalid Numeric Code '%i'.\n\n", num);
       return 0;
     }
     // add the temporary convered character string into the result string.
     strcat(str, tmp);
   }

   // display correct decoded text.
   printf("Decrypt Code: %s\n\n", str);
   return 1;
}//Decryption()

/*
  PRE-CONDITION:
    The conversion code is stored in the CODE array correctly.
  FUNCTION:
    Prompt user for a command to encrypt or to decrypt a text.
    tokenize the user into command and sentence.
    If user types invalid command, display an error message and keep prompt a new command.
    Otherwise, call encryptio() or decryption() function to encode or to decode the sentence from user command.
    Run the program until user types 'quit' or 'q' to exit the program.
  POST-CONDITION:
    displayed error message if user input is invalid.
    encoded or decoded text is displayed otherwise.
*/
void cryptography(CODE *code, int *count){

  char buff[1024], *command;

  // prompt for a command
  printf(">> ");
  fgets(buff, sizeof(buff), stdin);
  if(buff[strlen(buff)-1] == '\n'){ buff[strlen(buff)-1] = '\0'; }

  // While user did not want to exit the program
  while(strcmp(buff, "quit") && strcmp(buff, "q")){

    //Tokenization (split up the action command and the sentence)
    command = strtok(buff, " ");
    char* sentence = strtok(NULL, "");

    if((!strcmp(command, "encrypt") || !strcmp(command, "e")) && sentence != NULL){
      // if action command was 'encrypt' or 'e', call encryption function to encode the sentence
      encryption(code, count, sentence);
    }else if((!strcmp(command, "decrypt") || !strcmp(command, "d")) && sentence != NULL){
      // if action command was 'decrypt' or 'd', call decryption function to decode the sentence
      decryption(code, count, sentence);
    }else{
      // if user typed an invalid command, display an warning sign.
      printf("ERROR: Invalid Usage. <command> <sentence OR code>\n");
      printf("command: encrypt (or e), decrypt (or d), quit (or q)\n\n");
    }

    // prompt for a new command
    printf(">> ");
    fgets(buff, sizeof(buff), stdin);
    if(buff[strlen(buff)-1] == '\n'){ buff[strlen(buff)-1] = '\0'; }
  }//WHILE( NOT QUIT )
}//cryptography()

int main(int argc, char** argv){

  /* File Input Section */
  /* Extract the File */
  char filename[128];
  printf("Type Filename: ");
  fgets(filename, sizeof(filename), stdin);
  if(filename[strlen(filename)-1] == '\n'){ filename[strlen(filename)-1] = '\0'; }

  FILE *f = fopen(filename, "r");

  // Unable to Open the File Error
  if(f == NULL){
    printf("\nERROR: Unable to find/open a \"%s\" file.\n\n", filename);
    return 0;
  }

  int count = 0; // total number of conversion Code
  int *c = &count;
  CODE code[90]; // struct CODE array

  // Create Linked-List
  createList(f, code, c);
  fclose(f);
  /* End of File Input Seciton */

  // print out the conversion Code from teh file to verify if it's correctly stored.
  printArray(code, c);
  printf("Substition Key Setting is Completed!\n");
  usage(); // pring out the manuscript for the user
  cryptography(code, c); //start encoding and decoding.

  return 0;
}//main()
