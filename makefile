CC = gcc
CFLAGS = -g
SRCS = $(wildcard *.c)
PROGS = $(patsubst %.c,%,$(SRCS))

all: $(PROGS)

%: %.c
	$(CC) -std=c99 -o $@ $< $(CFLAGS)

clean:
	rm -f *.o
