﻿//Example program 5.6: Passing parameters by reference
#include <stdio.h>

//x is a pointer, *x is the value saved at where x is pointing
void swap(int *x, int *y)
{
	int xx,yy;

	xx = x[0];
	yy = y[0];
	x[0] = yy;
	y[0] = xx;

	return;
}

int main(void)
{
	int i = 15;
	int j = 10;

	printf_s("Before i: %d, j: %d\n", i, j);
	swap(&i, &j);//passing addresses of i and j to swap()
	printf_s("After  i: %d, j: %d\n", i, j);

	puts("Press Enter to exit program.");
	getchar();
	return 0;
}



