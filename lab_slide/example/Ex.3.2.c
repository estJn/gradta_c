﻿//Example Program 3.2: Same as Example 3.1 but using a sentinel value to end input
#include <stdio.h>

int main(void)
{
	unsigned int n;		//counter to show number of grades
	int grade;			//a student's grade
	unsigned int total;	//sum of graeds so far
	unsigned int average;//average average
					
	total = 0;			//initialize parameters total and n
	n = 0;
						//read a grade
	printf_s("Enter grade %d: ", n+1);
	scanf_s("%d", &grade); 
	while (grade>=0) {	//if nonzero
		n = n + 1;
		total = total + grade; //add to total
						//read another grade
		printf_s("Enter grade %u: ", n+1);
		scanf_s("%d", &grade);
	}
						//if 0 grades provided
	if (n == 0)puts("No grades provided.");
	else {				//when 1 or more grades provided
		average = total / n;	//find average grade
								//print average grade
		printf_s("Average grade of %u students is: %u\n", n, average);
	}					

	return 0;						
}