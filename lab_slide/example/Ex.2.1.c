﻿//Example Program 2.1: 1) Read an integer and a float, 2) add the numbers, and 3) print the result.

#include <stdio.h>								//header file, containing definitions for text i/o functions

//start of the main function
int main(void)
{
	int num1;									//declaring an integer
	float num2;									//declaring a float
	double num3;								//declaring a double-precision float

	puts("Enter an integer and a float.");		//displaying a message
	scanf_s("%d%f", &num1, &num2);				//reading an integer in num1 and a float in num2

	num3 = num1 + num2;							//adding num1 and num2 and saving in num3
												//printing num1, num2, and num3
	printf_s("Sum of %d and %g is %lg.\n", num1, num2, num3);

	return 0;									//returning 0 upon normal exit
}//end of the main function