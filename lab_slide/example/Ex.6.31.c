﻿//Example 6.3: Specifying the number of characters to read into a string
#include <stdio.h>		

int main(void)
{	//20 characters treated as two strings
	char name[20]; 
	char *first, *last;
	
	first = name;
	last = name + 10;
	puts("Enter first name followed by last name:");
	//read up to 9 characters into each string
	scanf_s("%9s%9s", first, 10, last, 10); getchar();
	printf_s("Name: %s %s\n", name, name+10);
	printf_s("First name: %s; Last name: %s\n", first, last);

	puts("Press ENTER to exit program.");
	getchar();

	return 0;
}