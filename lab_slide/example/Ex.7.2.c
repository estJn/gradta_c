﻿//Example 7.2: Passing parameters by value and by reference
#include <stdio.h>

//call by value
int callByVal(int n)
{
	return  n*n*n;
}

//call by reference
void callByRef(int n, int *n3)
{
	(*n3) = n*n*n;
}


int main(void)
{
	int num1 = 3, num2 = 5, num3;

	//call by value
	num3 = callByVal(num1);
	printf_s("%d to the power of 3 is %d.\n", num1, num3);
	//call by reference
	callByRef(num2, &num3);
	printf_s("%d to the powere of 3 is %d.\n", num2, num3);

	puts("Press ENTER to exit program.");
	getchar();

	return 0;
}
