﻿//Example program 5.3: Generate 10 random numbers between 0 and 1
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
	double a;

	// Function time(NULL) returns the number of seconds 
	// since midnight on January 1, 1970. Function srand() 
	// uses this time as the seed to generate other numbers.
	// This makes it possible to generate different random
	// numbers everytime the program is run.
	srand((unsigned char)time(NULL));
	puts("10 random numbers between 0 and 1");
	for (int i = 0; i < 10; i++) {
		a = (double)rand() / (double)RAND_MAX;
		printf_s("%lf\n", a);
	}
	puts("Press Enter to exit program.");
	getchar();
	return 0;
}

