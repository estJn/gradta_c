﻿//Example 7.1: Using the & and * pointer operators
#include <stdio.h>

int main(void)
{
	int a = 9;	//a is an integer variable
	int *ptr;	//ptr can point to an integer variable

	ptr = &a;	//saving address of variable a in ptr

	printf_s("Address of a is %p.\n", ptr); //printing an address
	printf_s("Value in memory pointed to by ptr is %d.\n", *ptr);

	puts("Press ENTER to exit program.");
	getchar();

	return 0;
}
