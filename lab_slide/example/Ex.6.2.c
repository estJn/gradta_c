﻿//Example 6.2: Initializing a 2-D array and finding the sum of its entries.
#include <stdio.h>		

int main(void)
{
	//Initialize scalars 
	int total = 0, n = 0;
	//Initialize array entries
	//Note that the last entry is set to 0
	int A[2][3] = { 1,2,3,5,7 };
	//Find sum of array entries and print
	printf_s("Sum of ");
	for (size_t row = 0; row < 2; ++row)
		for (size_t col = 0; col < 3; ++col)
		{
			n++;
			total += A[row][col];
			if (n < 6)
				printf_s("%d, ", A[row][col]);
			else
				printf_s("and %d ", A[row][col]);
		}
	printf_s("is %d.\n", total);

	puts("Press ENTER to exit program.");
	getchar();

	return 0;
}