﻿//Example 7.3: Finding the number of bytes in a data type with sizeof()
#include <stdio.h>

int main(void)
{
	char a;
	short b;
	int c;
	float d;
	double e;
	long double f[10];
	
	//The following two print_f() statements are equivalent.
	puts("Data types char, short, int, float, and double are");
	printf_s("composed of %u, %u, %u, %u, and %u bytes, respectively.\n\n", sizeof(char),
		sizeof(short),sizeof(int),sizeof(float), sizeof(double));
	
	puts("Variables a, b, c, d, and e are composed of");
	printf_s("%u, %u, %u, %u, and %u bytes, respectively.\n\n",
		sizeof(a), sizeof(b), sizeof(c), sizeof(d),sizeof(e));
	//sizeof() can be used to find the number of elements in an array
	printf_s("Array f has %u elements.\n\n", sizeof(f)/sizeof(f[0]));

	puts("Press ENTER to exit program.");
	getchar();

	return 0;
}
