﻿//Example Program 2.2: Demonstrating use of if-statement and type-casting

#include <stdio.h>

int main(void)
{
	int num1;
	float num, num2;
	double num3; 

	puts("Enter two floating-point numbers.");		
	scanf_s("%f%f", &num, &num2); 
	num1 = (int)num;					//Truncating the first float to an integer
	if ((float)num1 != num)
		printf_s("%g truncated to %d.\n", num, num1);
	else
		printf_s("%f is same as %d.\n", num, num1);
	num3 = num1 + num2;					//adding in float and implicitly type-casting to double
	printf_s("Sum of %d and %g is %lg.\n", num1, num2, num3);				

	return 0;						
}