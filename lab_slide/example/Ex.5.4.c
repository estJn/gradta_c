﻿//Example Program 5.4: An example of type coercion
#include <stdio.h>

//this function receives 2 short numbers and 
//returns the larger of the 2
int larger(short x, short y)
{
	if (x > y) return x;
	else return y;
}

//this function receives 3 integers and 
//returns the largest of the 3
int largest(int x, int y, int z)
{	//if x, y, and z are too large the
	//following function calls will
	//not produce correct results
	x = larger(x, y);
	y = larger(x, z);
	return y;
}

int main(void)
{
	long i, j, k;
	//read 3 numbers
	puts("Enter three integers.");
	scanf_s("%ld%ld%ld", &i, &j, &k); getchar();
	//find and print the largest of 3
	printf_s("The largest of %ld, %ld, and %ld is %d.\n",
		i, j, k, largest(i, j, k));
	//if i, j, and k are too large, the
	//returned value could be wrong
	puts("\nPress Enter to exit program.");
	getchar();

	return 0;
}

