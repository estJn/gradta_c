﻿//Example Program 5.2: Finding the largest of 3 numbers
#include <stdio.h>

//this function receives 2 integers and 
//returns the larger of the 2
int larger(int x, int y)
{
	if (x > y) return x;
	else return y;
}

//this function receives 3 numbers and 
//returns the largest of the 3
int largest(int x, int y, int z)
{
	x = larger(x, y);
	y = larger(x, z);
	return y;
}


int main(void)
{
	int i, j, k;
	//read 3 numbers
	puts("Enter three integers.");
	scanf_s("%d%d%d", &i, &j, &k); getchar();
	//find and print the largest of the 3
	printf_s("The largest of %d, %d, and %d is %d.\n",
		i, j, k, largest(i, j, k));

	puts("\nPress Enter to exit program.");
	getchar();
	return 0;
}
