﻿//Example 5.5: Another example of type coercion
#include <stdio.h>

//calculating factorial n using an unsigned long long argument
unsigned long long factorial(unsigned long long n)
{
	unsigned long long i;

	if (n == 0 || n == 1)
		return 1;
	else
		for (i = n - 1; i > 1; i--)
			n = n*i;
	return n;
}


int main(void)
{
	int n, done = 0;
	do {
		puts("Enter a number between 0 and 20:");
		scanf_s("%d", &n); getchar();
		//calling factorial, passing an integer argument
		if (n >= 0 && n <= 20)
			printf_s("Factorial of %d is %llu\n", n, factorial(n));
		else done = 1;
	} while (!done);

	puts("\nPress ENTER to exit program.");
	getchar();

	return 0;
}

