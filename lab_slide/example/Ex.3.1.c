﻿//Example Program 3.1: Using a while loop find average grade of 10 students
#include <stdio.h>

int main(void)
{
	unsigned int n;	//a counter used to show number of grades processed so far
	int grade;		//a student's grade
	int total;		//sum of graeds so far
	int average;	//average average

					//initialize parameters
	total = 0;
	n = 0;
					//while n<10 add grade to total		
	while (n < 10) {//read grades and add to total
		n = n + 1;
		printf_s("Enter grade %d: ",n);
		scanf_s("%d", &grade); 
		total = total + grade;
	}
					//find average grade
	average = total / n;
					//print average grade
	printf_s("Average grade is: %d\n", average);				

	return 0;						
}