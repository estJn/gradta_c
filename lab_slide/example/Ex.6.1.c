﻿//Example program 6.1:  Declaring and using arrays
#include <stdio.h>

int main(void)
{
	//two string arrays
	char first[20], last[20];
	//an integer array
	int grade[3];
	float total = 0, average;  //two scalars

	puts("Enter first and last names:");
	scanf_s("%19s%19s", first, 20, last, 20);
	getchar();

	puts("Enter CEG2170 grade:");
	scanf_s("%d", &grade[0]); getchar();
	puts("Enter CS1160 grade:");
	scanf_s("%d", &grade[1]); getchar();
	puts("Enter CEG2700 grade:");
	scanf_s("%d", &grade[2]); getchar();
	for (int i = 0; i < 3; i++)
		total += grade[i];
	average = total / 3;

	printf("Average grade for %s %s: %.1f.\n",
		first, last, average);

	puts("Press Enter to exit program.");
	getchar();
	return 0;
}



