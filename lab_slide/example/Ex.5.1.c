﻿//Example Program 5.1: Creating and using a function

#include <stdio.h>

/*This function receives an integer and
returns its square as a long integer. */
long int square(int x)
{
	long int y = x;
	return y*y;
}

int main(void)
{
	int i;
	long int j;

	puts(" x   x^2");
	puts("---------");
	for (i = 1; i <= 10; i++) {
		j = square(i); //let j be  square of i,
					   //then, print i and j
		printf_s("%2d %4ld\n", i, j);
	}
	puts("---------");
	puts("\nPress Enter to exit program.");
	getchar();
	return 0;
}

