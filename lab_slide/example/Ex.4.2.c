﻿//Example Program 4.2: Using a switch() statement to select one among many
#include <stdio.h>


int main(void)
{
	int coin;
	int done = 0;	//1: true, 0: false
	
	while(!done){
			puts("How many cents?");
			scanf_s("%d", &coin); getchar();
		switch (coin) {
		case 1:  puts("It's a penny");
				 break;
		case 5:  puts("It's a nickel");
				 break;
		case 10: puts("It's a dime");
				 break;
		case 25: puts("It's a quarter");
				 break;
		default: printf_s("No %d-cent coin\n", coin);
				done = 1;
				break;
		}
	} 
	puts("Press ENTER to exit program.");
	getchar();
	return 0;
}
