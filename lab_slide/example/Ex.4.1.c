﻿//Example Program 4.1: Using for-loop to calculate compound interests over 10 years
#include <stdio.h>
#include <math.h>

int main(void)
{
	double principal;
	double interest_rate;
	unsigned int noyears;
	double amount;
				//read in principal, interest rate, and number of years
	printf_s("Enter principal, interest rate, and number of years: ");
	scanf_s("%lf%lf%u", &principal,&interest_rate,&noyears); getchar();
				//print the table header
	printf_s("Amount of deposit annually for %d years.\n",noyears);
	puts("Year  Amount of deposit");
				//calculate and print the table showing annual deposit
	for (unsigned int year = 1; year <= noyears; year++) {
		amount = principal*pow(1.0+interest_rate, year);
		printf_s("%2u%15.2f\n", year, amount);
	}

	puts("Press Enter to exit program.");
	getchar();						

	return 0;						
}